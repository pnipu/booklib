from django.db import models
from django.urls import reverse # для генерации URL
from django.contrib.auth.models import User
import uuid  # для уникальности экземпляров книг
from datetime import date

# Create your models here.
class Genre(models.Model):
    """
    Модель представляет жанр книги
    """
    name = models.CharField(max_length=200, help_text="Введите название жанра (Фантастика, Поэзия)")


    def __str__(self):
        """
        String for represenitng Model object
        """

        return self.name


class Language(models.Model):
    """
    Представление языка книги
    """
    lang = models.CharField(max_length=100)
    lang_ru = models.CharField(max_length=100)
    
    def __str__(self):
        if self.lang == self.lang_ru:
            return '%s' % (self.lang)
        return '%s (%s)' % (self.lang, self.lang_ru)


class Book(models.Model):
    """
    Модель описывает книгу (но не конкретный экземпляр)
    """

    title = models.CharField(max_length=200)
    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True)
    # ForeignKey потому что какой-то автор эту книгу написал (но мог написать и другие)
    summary = models.TextField(max_length=1000, help_text="Введите краткое описание")
    isbn = models.CharField('ISBN', max_length=13, help_text='13 символов ISBN')
    genre = models.ManyToManyField(Genre, help_text="Выберите жанр для этой книги")
    lang = models.ForeignKey('Language', on_delete=models.SET_NULL, null=True)
    
    

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        """
        Возвращает URL для конкретного экземпляра книги
        """
        return reverse('book-detail', args=[str(self.id)])
    
    def display_genre(self):
        """
        Создает строку для отображения жанра
        """
        return ', '.join([genre.name for genre in self.genre.all()[:3]])
        
    display_genre.short_description = 'Genre'


class BookInstance(models.Model):
    """
    Представление экземпляра книги
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="Уникальный ИД в библиотеке")
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True)
    imprint = models.CharField(max_length=200)
    due_back = models.DateField(null=True, blank=True)

    LOAN_STATUS = (
        ('m', 'В ремонте'),
        ('o', 'Выдана'),
        ('a', 'Доступна'),
        ('r', 'Зарезервирована'),
    )

    status = models.CharField(max_length=1, choices=LOAN_STATUS, blank=True, default='m', help_text='Доступность книги')
    
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    
    @property
    def is_overdue(self):
        return self.due_back and date.today() > self.due_back


    class Meta:
        ordering = ["due_back"]
        permissions = (("can_mark_returned","Set book as returned"),)


    def __str__(self):
        return '%s (%s)' % (self.id, self.book.title)
    
    def display_title(self):
        """
        Создает строку для отображения названия книги
        """
        return self.book.title
        
    display_title.short_description = 'Title'
    
    def display_id(self):
        """
        Создает строку для отображения первых 5 символов id
        """
        return str(self.id)[:5]
        
    display_id.short_description = 'Id'


class Author(models.Model):
    """
    Представление автора
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    date_of_death = models.DateField('Died', null=True, blank=True)

    def get_absolute_url(self):
        """
        Возвращает адрес для доступа к конкретному экземпляру автора
        """
        return reverse('author-detail', args=[str(self.id)])


    def __str__(self):
        return '%s, %s' % (self.last_name, self.first_name)
    
    class Meta:
        ordering = ['last_name']

