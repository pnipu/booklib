from django.shortcuts import render
from .models import Author, Book, BookInstance, Genre
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin


def index(request):
    """
    Отображение домашней страницы
    """
    # Количество авторово, книг и экземпляров
    num_authors = Author.objects.count()
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()
    
    # Количество доступных книг
    num_availables = BookInstance.objects.filter(status='a').count()
    
    # Количество жанров
    num_genres = Genre.objects.all().count()
    
    # Количество книг, имеющих в названии слово Джордж
    num_georges_title = Book.objects.filter(title__contains='Джордж').count()
    
    
    # Количество посещений страницы с данного браузера (считается в session)
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1
    
    # Отрисовка HTML-шаблона index.html
    # данные внутри context
    
    ctx = {
        'num_books': num_books, 
        'num_instances': num_instances, 
        'num_availables': num_availables, 
        'num_authors': num_authors,
        'num_genres': num_genres,
        'num_georges_title': num_georges_title,
        'num_visits': num_visits
    }
    
    return render(
        request,
        'index.html',
        context=ctx
    )



class BookListView(generic.ListView):
    model = Book
    context_object_name = 'book_list'
    # queryset = Book.objects.filter(title__icontains='Джордж')[:2]  # получение двух книг, содержащих 'Джордж' в названии
    queryset = Book.objects.all()
    template_name = 'books/template_name_list.html'  # Нвзвание и расположение шаблона


class BookDetailView(generic.DetailView):
    model = Book


class AuthorListView(generic.ListView):
    model = Author
    context_object_name = 'author_list'
    queryset = Author.objects.all() 
    template_name = 'authors/template_name_list.html'  # Нвзвание и расположение шаблона


class AuthorDetailView(generic.DetailView):
    model = Author

class LoanedBooksByUserListView(LoginRequiredMixin, generic.ListView):
    """
    Generic class-based view список книг, на руках у данного читателя.
    """
    model = BookInstance
    template_name = 'catalog/bookinstance_list_borrowed_user.html'
    paginate_by = 10
    
    def get_queryset(self):
        """
        "o" - код для "on loan", сортировка по дате due_back, сначала старые элементы.
        """
        return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')

